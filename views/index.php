<!DOCTYPE html>
<html>
<head>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDosrroYUsBDXqUjkE-Ul-bwE7OP7vLyLU&libraries=places"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<title>Search</title>
</head>
<body>
	<div class="container">
		<h3>TODO</h3>
		<form>
			<div class="form-group">
				<input type="text" class="form-control" id="todo" placeholder="To Do.." required>
			</div>
			<div class="form-group">
				<input class="form-control" type="text" name="search" id="address" placeholder="Type address..." required>
			</div>
			<div class="">
				<select id="month" class="form-control" style="margin-bottom: 20px;" required>
					<option value=''>--Select Month--</option>
					<option value="JAN">JAN</option>
					<option value="FEB">FEB</option>
					<option value="MAR">MAR</option>
					<option value="APR">APR</option>
					<option value="MAY">MAY</option>
					<option value="JUN">JUN</option>
					<option value="JUL">JUL</option>
					<option value="AUG">AUG</option>
					<option value="SEPT">SEPT</option>
					<option value="OCT">OCT</option>
					<option value="NOV">NOV</option>
					<option value="DEC">DEC</option>
				</select>
			</div>
			<div class="form-group" style="{margin-bottom: 20px;}">
				<input class="form-control" type="datetime-local" id="userTime" placeholder="Select Time">
			</div>
			<input type="hidden" name="lat" id="lat">
			<input type="hidden" name="lng" id="long">
			<button class="btn btn-primary" type="submit" name="submit">Search</button>
		</form>
	</div>
	<script type="text/javascript">

		function init() {
			var searchInput = document.getElementById('address');
			var autocomplete = new google.maps.places.Autocomplete((searchInput), {
				types:['geocode']
			});

			autocomplete.addListener('place_changed', function () {
				var place = autocomplete.getPlace();

				document.getElementById('lat').value = place.geometry.location.lat();
				document.getElementById('long').value = place.geometry.location.lng();
			});
		}

		google.maps.event.addDomListener(window, 'load', init);

		$(function () {

			$('form').on('submit', function (e) {
				e.preventDefault();

				var todo = document.getElementById('todo').value;
				var address = document.getElementById('address').value;
				var month = document.getElementById('month').value;
				var time = document.getElementById('userTime').value;
				var lat = document.getElementById('lat').value;
				var long = document.getElementById('long').value;

				console.log(address);
				console.log(lat);
				console.log(long);

				$.ajax({
					url:'../controller/SearchAction.php',
					type:'POST',
					data: {todo:todo, address:address, month:month, time:time, lat:lat, lng:long},
					success:function(data)
					{
						alert(data);
					}
				});
			});
		});
	</script>
</body>
</html>
