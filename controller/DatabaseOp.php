<?php

class DatabaseOp extends connection
{
	
	protected $conn;

	function __construct()
	{
		$this->conn = parent::__construct();
	}

	public function insertAction($todo, $keyword, $month, $user_time, $lat, $long)
	{
		$insert_query = "INSERT INTO 
							search (`todo`, `keyword`, `month`, `lat`, `lng`, `created_at`) 
						 VALUES (:todo, :keyword, :month, :lat, :lng, :created_at)";

		$insert_prepare = $this->conn->prepare($insert_query);
		$insert_prepare->bindValue(':todo', $todo);
		$insert_prepare->bindValue(':keyword', $keyword);
		$insert_prepare->bindValue(':month', $month);
		$insert_prepare->bindValue(':lat', $lat);
		$insert_prepare->bindValue(':lng', $long);
		$insert_prepare->bindValue(':created_at', $user_time);

		$result = $insert_prepare->execute();

		if (!empty($result)) {
			return true;
		} else {
			return false;
		}
	}
}