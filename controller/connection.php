<?php

class connection
{

	protected $host = "localhost";
	protected $username = "root";
	protected $password = "";
	protected $db_name = "ajackus";

	protected $conn;

	//// FUNCTION FOR CONNECTING TO DATABASE ////

	function __construct()
	{
		try {
			$this->conn = new PDO("mysql:host=".$this->host.";dbname=".$this->db_name, $this->username, $this->password);
			// set the PDO error mode to exception
			
			// $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch(PDOException $e) {
			echo "Connection failed: " . $e->getMessage();
		}

		return $this->conn;
	}
}