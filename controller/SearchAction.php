<?php
ini_set('display_error', 0);
error_reporting(E_ALL);

function __autoload($file)
{
	require_once "$file.php";
}

$user_todo = $_POST['todo'];
$keyword = $_POST['address'];
$month = $_POST['month'];
$user_time = $_POST['time'];
$lat = $_POST['lat'];
$long = $_POST['lng'];

$db_op = new DatabaseOp();
if (!empty($keyword) && !empty($lat) && !empty($long) && !empty($month)) {

	$insert_result = $db_op->insertAction($user_todo, $keyword, $month, $user_time, $lat, $long);

	if ($insert_result == true || $insert_result == 'true') {
		echo "Here is the search Data: 
			{
				To Do: $user_todo,
				Place: $keyword,
				Month: $month,
				Latitude: $lat,
				Longitude: $long
			}";
	} else {
		echo "Data is not Inserted!";
	}
} else {
	echo "Didn't search your Address!";
}
